/**   
* Filename:    CenterController.java   
* Create at:   2016年3月28日 上午9:52:19   
* Description:  
* Modification History:   
* Author      Version     Description   
* ----------------------------------------------------------------- 
* Garfield      1.0       1.0 Version   
*/ 
package pga2;
/**
 * Center Controller
 * @author Garfield
 * @data 2016年3月28日
 * @Emial:344892053@qq.com
 */
public class CenterController {
	public static void main(String[] args) {
		Crawler.RunCrawler(args);
	}
}
