/**   
* Filename:    TimeSec.java   
* Create at:   2016年3月28日 下午3:19:28   
* Description:  
* Modification History:   
* Author      Version     Description   
* ----------------------------------------------------------------- 
* Garfield      1.0       1.0 Version   
*/ 
package pga2.tools;

public class TimeSec {
	public static void Timesec(int time) {
		try {
			Thread thread = Thread.currentThread();
			thread.sleep(time);
		} catch (InterruptedException e) {
			System.out.println("TIMESEC ERROR!Time data must be Int!");
		}
	}
}
