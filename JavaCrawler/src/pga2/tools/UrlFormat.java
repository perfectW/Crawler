package pga2.tools;

import java.util.concurrent.SynchronousQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlFormat {
public static void main(String[] args) {
		
//		System.out.println(GetUrlHeader("http://www.51nes.cn/nesgame/ACT/neslist1.htm"));
	}
	
	/**
	 * 判断是否是网址
	 * 
	 * @author puguoan
	 * @param zhengzhebiaodashi
	 * @return
	 */
	public static boolean IsUrl(String zhengzhebiaodashi) {
		Pattern p = Pattern
				.compile("(http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?");
		Matcher m = p.matcher(zhengzhebiaodashi);
		if (!m.matches()) {
			System.out.println("错误");
			return true;
		} else {
			System.out.println("正确");
			return false;
		}
	}

	
	
	
	/**
	 * 网址判断
	 */
	public static boolean containstr(String str){
		String[] s = { "http://" ,"ftp://","https://"};
	        boolean flag=false;
	        for (int i = 0; i < s.length; i++) {
	            if (str.contains(s[i])) {
	                flag = true;
	            }else {
	            }
	        }
	        if(flag == true)
	        {
	        	return true;
	        }else{
	        return false;
	        }
	    }
	
	/**
	 * 获取URL的后缀
	 * @param url
	 * @return
	 */
	public static String GetSuffix(String url){
		int x = url.lastIndexOf("/");
		url = url.substring(x+1, url.length());
		return url;
	}
	
	
	
	
	// 字段与数组的判断
	public static boolean isIn(String substring, String[] source) {
		if (source == null || source.length == 0) {
			return false;
		}
		for (int i = 0; i < source.length; i++) {
			String aSource = source[i];
			if (aSource.equals(substring)) {
				return true;
			}
		}
		return false;
	}
	/**
	 * 获取Http传送协议  Http https Ftp
	 * @param url
	 * @return
	 */
	public static String GetHttpHead(String url){
		if(url.indexOf("http://") != -1){
			return "http://";
		}else if(url.indexOf("https://")!= -1){
			return "https://";
		}
		return url;
	}
	
	//保留  没用
	public static String Urlformat(String taskurl,String crawlUrl){
		String a = taskurl;
		String url = crawlUrl;
		String url_temp_head = UrlFormat.GetHttpHead(a);
		String tempurlRoot = StringUtils.GetUrlHeader(a);
		
		if (StringUtils.IsUrl(url)) {
			return url;
		} else {
			if (StringUtils.IsLink(url)) {
				if (url.indexOf("//") != -1) {
					url = url.replace("//", url_temp_head);
					return url;
				} else if (url.substring(0, 1).indexOf("/") != -1) {
					url = tempurlRoot + "@" +url;
					url = url.replace("@/","");
					return url;
				} else {
					url = a + "/@" +url;
					url = url.replace("//@", "/");
					url = url.replace("/@","/");
					return url;
				}
			}
		}
		return url;
	}
	
	public static String URLINPUTFORMAT(String url){
		if(url.indexOf("http://") != -1){
			return url;
		}else if(url.indexOf("https://") != -1){
			return url;
		}
		url = "http://"+url;
		return url;
	}
}
