/**   
* Filename:    SYSINFO.java   
* Create at:   2016年3月28日 下午1:13:40   
* Description:  
* Modification History:   
* Author      Version     Description   
* ----------------------------------------------------------------- 
* Garfield      1.0       1.0 Version   
*/ 
package pga2.tools;

public class SYSINFO {
	public static final String INITINFO = "Please enter the correct operation order!";
	
	public static final String HELPINFO = "=========================================== \r\n"
										+ "= Manipulator Crawler V 1.0.1 \r\n"
										+ "= @Update See:http://www.garfields.cc \r\n"
										+ "= @Author Puga 344892053@qq.com \r\n"
										+ "==========Other data======================= \r\n"
										+ "= {-header User-Agent@Android} {-cookie CookieValue} \r\n"
										+ "= {-data user=x&pass=x} {proxy IP:PORT} {-timesec time} {-post}\r\n"
										+ "=========================================== \r\n"
										+ "  cmd		DESC	\r\n"
										+ "   -v	Manipulator Crawler Version \r\n"
										+ "   -h	Manipulator Crawler UseINFO FAQ \r\n"
										+ "   -u   [url] {other data} \r\n"
										+ "   -cw  [url] -cq k,v@k,v k=title v=div[class=XXX] {other data}\r\n"
										+ "   -ci  -file <listpath> -input <inputpaht> -cq k,v@k,v... {other data} ";
	
	public static final String VERSIONINFO = "====================\r\n"
											+"= Manipulator Crawler V 1.0.1 \r\n"
											+"= Download:www.garfields.cc \r\n"
											+"====================";
}
