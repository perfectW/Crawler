package pga2.tools;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
	/**
	 * 判断是否为空   null和空格都是属于空值
	 * @param str1 字符串
	 * @return 
	 */
	public static boolean IsBlank(String str1){
		if(str1 ==null){
			return true;
		}
		str1 = str1.trim();
		if(str1.equals("")){
			return true;
		}
		return false;
	}
	
	/**
	 * 判断两个字符串是否相等
	 * @param str1 第一个字符串
	 * @param str2 第二个字符串
	 * @return
	 */
	public static boolean IsEqual(String str1,String str2){
		if(str1 ==null){
			str1 = "null";
		}
		if(str2 ==null){
			str2 = "null";
		}
		str1 = str1.trim();
		str2 = str2.trim();
		if(str1.equals(str2)){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 判断不是网址
	 * 
	 * @author puguoan
	 * @param url
	 * @return
	 */
	public static boolean IsUrl(String url) {
		Pattern p = Pattern
				.compile("(http|ftp|https|):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?");
		Matcher m = p.matcher(url);
		if (!m.matches()) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * 提取URL链接头   http://www.poist.com/XXXX  提取http://www.poist.com
	 * @param url  URL
	 * @return
	 */
	public static String GetUrlHeader(String url){
		if(!IsUrl(url)){
			System.err.println("请输入正确的URL!");
			System.exit(0);
		}
		String urlformat = url;
			boolean a = urlformat.contains("/");
			if(a){
				urlformat = urlformat + "/";
			}
		if(urlformat.indexOf("/", 8) == -1){
			return urlformat;
		}
		urlformat = urlformat.substring(0, urlformat.indexOf("/", 8))+"/";
		return urlformat;
	
	}

	/**
	 * 转换"//"的问题
	 * @param url
	 * @return
	 */
	public static String GetUrlFormat(String url){
		if(url.indexOf("//", 8) == -1){
			return url;
		}else{
			url = url.replace("http://", "").replace("//", "/");
			url = "http://"+url;
			return url;
		}
	}
	
	/**
	 * 判断是否为正确的URL地址参数
	 * 
	 * @author puguoan
	 * @param url
	 * @return
	 */
	public static boolean IsLink(String url) {
		String[] guolvqi = {"*",":","^","#",";","@"};
		return isNotIn(url, guolvqi);
	}
	
	/**
	 * 是否包含  包含false 不包含True
	 * @param substring
	 * @param source
	 * @return
	 */
	public static boolean isNotIn(String substring, String[] source) {
		if (source == null || source.length == 0) {
			return true;
		}
		for (int i = 0; i < source.length; i++) {
			if (substring.indexOf(source[i]) != -1) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * LIST去重
	 * @param list
	 * @return
	 */
	public static List removeDuplicateWithOrder(List list) {
        Set set = new HashSet();
        List newList = new ArrayList();
        for (Iterator iter = list.iterator(); iter.hasNext();) {
            Object element = iter.next();
            if (set.add(element))
                newList.add(element);
        }
        return newList;
    }
	
		
		
	
}
