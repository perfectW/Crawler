/**   
 * Filename:    TxtCreateTools.java   
 * Create at:   2015年10月20日 下午2:21:26   
 * Description:  
 * Modification History:   
 * Author      Version     Description   
 * ----------------------------------------------------------------- 
 * Garfield      1.0       1.0 Version   
 */
package pga2.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CreateFile{
	/**
	 * @author puguoan
	 * @param FilePath
	 * @param node
	 */
	public static void makeTxt(String FilePath, String node) {
		FileWriter fw = null;
		try {
			// 如果文件存在，则追加内容；如果文件不存在，则创建文件
			File f = new File(FilePath);
			fw = new FileWriter(f, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		PrintWriter pw = new PrintWriter(fw);
		pw.println(node);
		pw.flush();
		try {
			fw.flush();
			pw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	

}
